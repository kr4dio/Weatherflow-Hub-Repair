# Weatherflow Hub Repair

## Background

My Weatherflow hub stopped working, and I couldn't find any relevant answers on their support forum. It turns out if you ask a question about actually broken equipment, they turn it into a private message so no one else can see what's going on.

Another user reports the following from tech support when his $300+ weather station died:

> "The overall failure rate of the Tempest is very low, and your Tempest had a solid run of about 28 months before dying."

Weatherflow customer service offered to sell me a new hub for $50.

I opened up my hub and found some useful test points. The red module (AES-BCM4343W-M1-G) is the main microcontroller with wifi and BLE radios, and the module with the metal shield (WXFLW001, containing EFR32) is the radio that actually listens for the Weatherflow sensors. The two modules communicate via I2C.

![Photo of the weatherflow hub pcb with I2C and serial connections labeled](hub-teardown.jpg)

## Easy Fix

After spending a little time trying to decode the i2c data being transfered between the two modules, I decided the easiest fix would be to extract the plaintext sensor data from the main controller's serial debug output. I connected a wire from J2 on the hub board to pin 3 (UART RX) of a Wemos D1 Mini module. I used the 5-volt power and ground connections at the legs of the large 3.3v regulator to supply the D1 Mini.

The included ESPHome YAML file contains a lambda function that listens for serial data enclosed in curly braces, then broadcasts that data on UDP port 50222 just like the original hub did. Tools such as [wfudptools](https://github.com/vinceskahan/wfudptools) and [weatherflow-udp](https://github.com/captain-coredump/weatherflow-udp) will be able to receive the UDP broadcasts and convert the data to mqtt, influxdb, weewx, etc.

## TODO

* Timestamps in the sensor data packets are wrong.
  * Replace timestamps before forwarding to UDP?
  * Home Assistant integration doesn't seem to use these anyway
* Parse data packets and output decoded data
  * MQTT?

## Reference Materials

* [WXFLW001 FCC Testing Info](https://fccid.io/2AEQB-WXFLW001)
* [WXFLW001 User Manual](reference/User-manual-3652537.pdf)
* [EFR32 Datasheet](reference/efr32fg12-datasheet.pdf)
* [AES-BCM4343W-M1-G Datasheet](reference/AES-BCM4343W-M1-G_data_sheet_v2_3.pdf)